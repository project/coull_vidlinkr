<?php

/**
 * @file
 * Install routines and requirements hook
 */

/**
 * Implements hook_install().
 */
function coull_vidlinkr_install() {
  $t = get_t();

  // Output a message immediately after install asking the user to configure the module
  drupal_set_message($t('Please visit the <a href="!config_url">Coull Vidlinkr configuration page</a> to connect your site to the Coull Vidlinkr API', array('!config_url' => url(COULL_VIDLINKR_ADMIN_PATH))), 'warning');
}

/**
 * Implements hook_requirements().
 */
function coull_vidlinkr_requirements($phase) {
  $t = get_t();

  if ($phase === 'runtime') {

    // Attempt to fetch any stored config
    $vidlinkr = new Vidlinkr(variable_get('coull_vidlinkr_config', NULL));

    // Check to see whether a Vidlinkr API key is set.
    if (!is_array($vidlinkr->config) || !isset($vidlinkr->config[Vidlinkr::CONFIG_KEY_PRIVATE_API_KEY])) {

      // Ask users to configure the module if no api key is set in the config
      return array(
        'vidlinkr' => array(
          'title' => $t('Coull Vidlinkr API key'),
          'value' => $t('Not set'),
          'description' => $t('Please visit the <a href="!config_url">Coull Vidlinkr configuration page</a> to connect your site to the Coull Vidlinkr API', array('!config_url' => url(COULL_VIDLINKR_ADMIN_PATH))),
          'severity' => REQUIREMENT_WARNING,
        ),
      );

    }
    else {

      // If config *is* set, try and validate it
      try {

        // Is the API key valid?
        Vidlinkr::verifyPrivateApiKey($vidlinkr->config[Vidlinkr::CONFIG_KEY_PRIVATE_API_KEY]);

        // No exceptions thrown, the API key must be valid.
        return array(
          'vidlinkr' => array(
            'title' => $t('Coull Vidlinkr API key'),
            'value' => Vidlinkr::getObscuredApiKey($vidlinkr->config[Vidlinkr::CONFIG_KEY_PRIVATE_API_KEY]),
            'severity' => REQUIREMENT_OK,
          ),
        );

      } catch (Vidlinkr_Exception $e) {

        // The vidlinkr api threw an exception, catch it and output the error
        return array(
          'vidlinkr' => array(
            'title' => $t('Coull Vidlinkr API key'),
            'value' => Vidlinkr::getObscuredApiKey($vidlinkr->config[Vidlinkr::CONFIG_KEY_PRIVATE_API_KEY]),
            'description' => $e->getMessage(),
            'severity' => REQUIREMENT_ERROR,
          ),
        );

      }
    }
  }
}
