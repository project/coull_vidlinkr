<?php

require_once 'Exception.php';

class Vidlinkr_Api
{

	const STATUS_OK						= 200;
	const STATUS_OK_NO_CONTENT			= 204;
	const STATUS_BAD_REQUEST			= 400;
	const STATUS_NOT_FOUND				= 404;
	const STATUS_INTERNAL_SERVER_ERROR	= 500;

	const VIDEO_PROVIDER_ID_YOUTUBE		= 2;

	private static $_aDefaultCurlOpt = array (
		CURLOPT_CONNECTTIMEOUT	=> 5,
		CURLOPT_TIMEOUT			=> 5,
		CURLOPT_POST			=> true,
		CURLOPT_FOLLOWLOCATION	=> false,
		CURLOPT_RETURNTRANSFER	=> true,
		CURLOPT_SSL_VERIFYPEER	=> false
	);

	const API_HOST				= 'network.coull.com';
	const EMBED_HOST			= 'mash.network.coull.com';
	const USE_SSL				= true;
	const APPLICATION_API_KEY	= 'jOufroebrluylujoavlUtrietHI9spleswiegiupriaslabi';

	public static function doRequest ($sMethod, $aParam=array(), $iExpectedStatus=200)
	{

		$aParam['format'] = 'json';
		$aParam['application_api_key'] = self::APPLICATION_API_KEY;

		$aCurlOpt = self::$_aDefaultCurlOpt;

		$aCurlOpt[CURLOPT_URL] = 'http' . ((true == self::USE_SSL) ? 's' : '') . '://' . self::API_HOST . '/api/' . $sMethod;
		$aCurlOpt[CURLOPT_POSTFIELDS] = $aParam;

		$oCh = curl_init();

		curl_setopt_array($oCh, $aCurlOpt);

		if (false == ($sResponse = curl_exec($oCh))) {

			switch (curl_errno($oCh)) {

				case CURLE_OPERATION_TIMEDOUT:
					$sErrorMessage = 'Attempt to contact the server timed out. Please try again later.';
					break;

				default:
					$sErrorMessage = 'cURL error: ' . curl_error($oCh);

			}

			throw new Vidlinkr_Exception($sErrorMessage);

		}

		$iHttpStatus = curl_getinfo($oCh, CURLINFO_HTTP_CODE);

		if ($iExpectedStatus != $iHttpStatus) {
			throw new Vidlinkr_Exception('The API server returned unexpected status: ' . $iHttpStatus);
		}

		/**
		* Remove callback if present
		*/
		if (true == mb_ereg('^[a-zA-Z0-9_\.]+\((.*)\);?$', $sResponse, $aMatch)) {
			$sResponse = $aMatch[1];
		}

		if (false == ($oJson = json_decode($sResponse))) {
			throw new Vidlinkr_Exception('The API response is invalid because it cannot be parsed as JSON');
		}

		if (false == isset($oJson->response)) {
			throw new Vidlinkr_Exception('The API response is invalid because it does not contain a "response" object');
		}

		$oResponse = $oJson->response;

		if (false == isset($oResponse->status->code)) {
			throw new Vidlinkr_Exception('The API response did not include a status');
		}

		if (self::STATUS_INTERNAL_SERVER_ERROR == $oResponse->status->code) {
			throw new Vidlinkr_Exception('The API reported an internal server error. Please try again later.');
		}

		return $oResponse;

	}

	function getResponseMessage ($oResponse)
	{
		return join(': ', array($oResponse->status->text, $oResponse->status->detail));
	}

}
