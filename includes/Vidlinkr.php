<?php

require_once 'Api.php';
require_once 'Exception.php';

/**
 * Vidlinkr Class
 */
class Vidlinkr
{
	public $config;

	const CONFIG_OPTION_NAME					 = 'coull_vidlinkr_config';

	const CONFIG_KEY_PRIVATE_API_KEY   = 'private_api_key';
	const CONFIG_KEY_PUBLISHER_ID      = 'publisher_id';
	const CONFIG_KEY_USER_LOGIN_ID     = 'user_login_id';
	const CONFIG_KEY_WEBSITE_ID        = 'website_id';
	const CONFIG_KEY_WEBSITE_URL       = 'website_url';

	public function __construct($aConfig = null) {
		$this->config = $aConfig;
	}

	/**
	* Verify the provided private API key, returning true if it is valid,
	* or throwing an exception otherwise.
	*
	* @param string $sApiKey
	* @return bool
	* @throws Vidlinkr_Exception
	*/
	public static function verifyPrivateApiKey ($sApiKey)
	{

		$oResponse = Vidlinkr_Api::doRequest('open/login', array('private_api_key'=>$sApiKey));

		if (Vidlinkr_Api::STATUS_BAD_REQUEST == $oResponse->status->code) {
			throw new Vidlinkr_Exception('The API key provided is not valid');
		}

		if (Vidlinkr_Api::STATUS_OK != $oResponse->status->code) {
			throw new Vidlinkr_Exception(Vidlinkr_Api::getResponseMessage($oResponse));
		}

		return true;
	}

	public function getPrivateApiKeyFromLogin ($sEmail, $sPassword)
	{

		$oResponse = Vidlinkr_Api::doRequest('open/login', array('email'=>$sEmail,'password'=>$sPassword));

		if (Vidlinkr_Api::STATUS_BAD_REQUEST == $oResponse->status->code) {
			throw new Vidlinkr_Exception('The login details provided are not valid');
		}

		if (Vidlinkr_Api::STATUS_OK != $oResponse->status->code) {
			throw new Vidlinkr_Exception(Vidlinkr_Api::getResponseMessage($oResponse));
		}

		if (false == isset($oResponse->data->session->token)) {
			throw new Vidlinkr_Exception('Login did not return a session token');
		}

		$oResponse = Vidlinkr_Api::doRequest('account/getprivateapikey', array('session_token'=>$oResponse->data->session->token));

		if (Vidlinkr_Api::STATUS_OK != $oResponse->status->code) {
			throw new Vidlinkr_Exception(Vidlinkr_Api::getResponseMessage($oResponse));
		}

		if (false == isset($oResponse->data->private_api_key)) {
			throw new Vidlinkr_Exception('Request did not return a private API key');
		}

		$this->config[self::CONFIG_KEY_PRIVATE_API_KEY] = $oResponse->data->private_api_key;

		return $oResponse->data->private_api_key;

	}

	/**
	* Fetch the details for the website identified by $sUrl, adding them to the
	* local config and returning true if found, otherwise returning false.
	* or throwing an exception otherwise.
	*
	* @param string $sUrl
	* @return bool
	* @throws Vidlinkr_Exception
	*/
	public function getWebsite ($sUrl)
	{

		$oResponse = Vidlinkr_Api::doRequest('account/getwebsite', array(
			'private_api_key'	=> $this->config[self::CONFIG_KEY_PRIVATE_API_KEY],
			'url'				=> $sUrl
		));

		if (Vidlinkr_Api::STATUS_OK == $oResponse->status->code) {

			$this->config[self::CONFIG_KEY_WEBSITE_ID] = $oResponse->data->website->id;
			$this->config[self::CONFIG_KEY_WEBSITE_URL] = $oResponse->data->website->url;
			$this->config[self::CONFIG_KEY_PUBLISHER_ID] = $oResponse->data->website->account_id;

			return TRUE;

		}

		if (Vidlinkr_Api::STATUS_NOT_FOUND == $oResponse->status->code) {
			return false;
		}

		throw new Vidlinkr_Exception(Vidlinkr_Api::getResponseMessage($oResponse));

	}

	/**
	* Add the website identified by $sUrl to, returning true if all went
	* well, otherwise throwing an exception.
	*
	* @param string $sUrl
	* @return bool
	* @throws Vidlinkr_Exception
	*/
	public function addWebsite ($sUrl)
	{

		$oResponse = Vidlinkr_Api::doRequest('account/addwebsite', array(
			'private_api_key'   => $this->config[self::CONFIG_KEY_PRIVATE_API_KEY],
			'url'			   => $sUrl
		));

		if (Vidlinkr_Api::STATUS_OK != $oResponse->status->code) {
			throw new Vidlinkr_Exception(Vidlinkr_Api::getResponseMessage($oResponse));
		}

		$this->config[self::CONFIG_KEY_WEBSITE_ID] = $oResponse->data->website->id;
		$this->config[self::CONFIG_KEY_WEBSITE_URL] = $oResponse->data->website->url;
		$this->config[self::CONFIG_KEY_PUBLISHER_ID] = $oResponse->data->website->account_id;

		return true;

	}

	/**
	* Wrapper for individual methods for each provider, testing that
	* publisher id and website id are both set before making changes.
	*
	* @param string $sHtml
	* @return string
	*/
	public function swapUrls ($sHtml)
	{

		/**
		* If either publisher id or website id is null, return the content unchanged
		*/
		if (null == $this->config[self::CONFIG_KEY_PUBLISHER_ID] || null == $this->config[self::CONFIG_KEY_WEBSITE_ID]) {
			return $sHtml;
		}

		return $this->_swapYouTubeUrls($sHtml);

	}

	/**
	* Swap out YouTube URLs
	*
	* @param string $sHtml
	* @return string
	*/
	private function _swapYouTubeUrls ($sHtml)
	{

		static $sRegexVideoId = '[0-9A-Za-z_-]+';
		static $sRegexMainHost = '(?:https?://|(?<!http:|https:)//)(?:www\.)?youtube\.com';
		static $sRegexShortHost = '(?:https?://|(?<!http:|https:)//)(?:www\.)?youtu\.be';

		static $sRegexIframe = '(<iframe[^>]+)';

		$sApiUrlYouTube = sprintf('http://%s/activatevideo?video_provider_id=%d&pid=%s&website_id=%s&video_provider_url=',
			Vidlinkr_Api::EMBED_HOST,
			Vidlinkr_Api::VIDEO_PROVIDER_ID_YOUTUBE,
			$this->config[self::CONFIG_KEY_PUBLISHER_ID],
			$this->config[self::CONFIG_KEY_WEBSITE_ID]
		);

		$sEvalString = '"$1".$sApiUrlYouTube.urlencode("$2")';

		// e.g. http://www.youtube.com/watch?v=Js7HYv-14v8&feature=xxx
		$sHtml = preg_replace('#' . $sRegexIframe . '(' . $sRegexMainHost . '/watch?.*v=(' . $sRegexVideoId . '))#ie', $sEvalString, $sHtml);

		// e.g. http://www.youtube.com/embed/Js7HYv-14v8
		// e.g. http://www.youtube.com/v/Js7HYv-14v8
		$sHtml = preg_replace('#' . $sRegexIframe . '(' . $sRegexMainHost . '/(?:embed|v)/(' . $sRegexVideoId . '))#ie', $sEvalString, $sHtml);

		// e.g. http://youtu.be/Js7HYv-14v8
		$sHtml = preg_replace('#' . $sRegexIframe . '(' . $sRegexShortHost . '/(' . $sRegexVideoId . '))#ie', $sEvalString, $sHtml);

		return $sHtml;

	}

	/**
	* Obscure an API key, leaving only the first and last three characters visible.
	*/
	public static function getObscuredApiKey ($sPrivateApiKey)
	{
		return preg_replace('#^(.{3})(.*)(.{3})$#e', "\"$1\".str_repeat('*',strlen('$2')).\"$3\"", $sPrivateApiKey);
	}

}
