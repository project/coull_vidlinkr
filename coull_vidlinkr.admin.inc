<?php

/**
 * @file
 * Vidlinkr admin callbacks
 */

/**
 * Vidlinkr admin page callback
 */
function coull_vidlinkr_admin_form($form, &$form_state) {

  // Grab our stored config and initialise a Vidlinkr object
  $vidlinkr = new Vidlinkr(variable_get('coull_vidlinkr_config', NULL));

  // If we have some config, attempt to validate the stored private key,
  if ($vidlinkr->config) {
    try {
      // Validate the key
      Vidlinkr::verifyPrivateApiKey($vidlinkr->config['private_api_key']);
    } catch (Vidlinkr_Exception $e) {
      // Alert the user that their private key wasn't valid
      drupal_set_message($e->getMessage(), 'warning');

      // If it's not valid, proceed as if there is no config to allow the user to
      // reauthenticate with new account details
      $vidlinkr->config = null;
    }
  }

  // Grab paths to this module
  $module_path = drupal_get_path('module', 'coull_vidlinkr');
  $assets_path = $module_path . '/assets';

  // Attach the css file for this form
  $form['#attached'] = array(
    'css' => array(
      $assets_path . '/coull_vidlinkr.css',
    ),
  );

  // Vidlinkr logo
  $form['logo'] = array(
    '#markup' => theme('image', array(
      'path' => $assets_path . '/coull_vidlinkr.png',
      'alt' => 'Coull: Vidlinkr',
      'attributes' => array(
        'class' => array(
          'vidlinkr-logo',
        ),
      ),
    )),
    '#weight' => -10,
  );

  // Build the form differently depending on whether we have config set
  if (!$vidlinkr->config) {

    // Fieldset for the main login form
    $form['api_authentication'] = array(
      '#type' => 'fieldset',
      '#title' => t('Connect to Vidlinkr'),
      '#description' => t('Sign into your Vidlinkr account to connect it to your site. Signing in will automatically start the process of adding Vidlinks to your videos.'),
      '#tree' => TRUE,
    );

    // Short blurb about signing up if you don't have an account
    $form['api_authentication']['sign_up'] = array(
      '#markup' => '<div class="vidlinkr-message">' . t('Don\'t have a Vidlinkr account yet? <a href="!find_out_more">Find out more</a> or <a href="!create_an_account">create an account now</a> (it\'s 100% free!', array('!find_out_more' => 'http://coull.com/vidlinkr/', '!create_an_account' => 'https://vidlinkr.coull.com/signup?referer=drupalmodule')) . '</div>',
    );

    // Email field
    $form['api_authentication']['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#attributes' => array(
        'placeholder' => t('Email'),
      ),
      '#required' => TRUE,
    );

    // Password field
    $form['api_authentication']['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#attributes' => array(
        'placeholder' => t('Password'),
      ),
      '#required' => TRUE,
    );

    // Submit button actions wrapper
    $form['api_authentication']['actions'] = array(
      '#type' => 'actions',
    );

    // Main submit button
    $form['api_authentication']['actions']['connect'] = array(
      '#type' => 'submit',
      '#value' => t('Connect to Vidlinkr'),
    );


  }
  else {

    // Output a message indicating the current connection status
    $form['connected'] = array(
      '#markup' => '<p>' . t('You are connected to Vidlinkr as %user', array('%user' => $vidlinkr->config[Vidlinkr::CONFIG_KEY_USER_LOGIN_ID])) . '</p>',
    );

    // Link to vidlinkr dashboard
    $form['vidlinkr_dashboard'] = array(
      '#markup' => l(t('Manage your videos with the Vidlinkr dashboard'), 'https://vidlinkr.coull.com/sites/videos/' . $vidlinkr->config[Vidlinkr::CONFIG_KEY_WEBSITE_ID] . '?referer=drupalmodule', array('attributes' => array('target' => '_blank', 'class' => array('button')))),
    );

    // Fieldset to display the stored config details.
    $form['api_config'] = array(
      '#type' => 'fieldset',
      '#title' => t('Show connection settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    // Table of config details
    $form['api_config']['details'] = array(
      '#theme' => 'table',
      '#header' => array(
        t('Setting'),
        t('Value'),
      ),
      '#rows' => array(),
    );

    // Email table row
    $form['api_config']['details']['#rows'][] = array(
      t('Email'),
      check_plain($vidlinkr->config[Vidlinkr::CONFIG_KEY_USER_LOGIN_ID]),
    );

    // Private key table row
    $form['api_config']['details']['#rows'][] = array(
      t('Private API key'),
      check_plain(Vidlinkr::getObscuredApiKey($vidlinkr->config[Vidlinkr::CONFIG_KEY_PRIVATE_API_KEY])),
    );

    // Pubisher ID table row
    $form['api_config']['details']['#rows'][] = array(
      t('Publisher ID'),
      check_plain($vidlinkr->config[Vidlinkr::CONFIG_KEY_PUBLISHER_ID]),
    );

    // Website ID table row
    $form['api_config']['details']['#rows'][] = array(
      t('Website ID'),
      check_plain($vidlinkr->config[Vidlinkr::CONFIG_KEY_WEBSITE_ID]),
    );

    // Website URL table row
    $form['api_config']['details']['#rows'][] = array(
      t('Website URL'),
      check_plain($vidlinkr->config[Vidlinkr::CONFIG_KEY_WEBSITE_URL]),
    );

    // Disconnect button actions wrapper
    $form['actions'] = array(
      '#type' => 'actions',
    );

    // Disconnect button
    $form['actions']['disconnect'] = array(
      '#type' => 'button',
      '#submit' => array('coull_vidlinkr_admin_form_disconnect'),
      '#executes_submit_callback' => TRUE,
      '#value' => t('Disconnect from your Vidlinkr account'),
      '#suffix' => '<div class="description">' . t('This will stop Vidlinks from appearing on your videos') . '</div>',
    );
  }

  return $form;
}

/**
 * Validate callback for the Vidlinkr admin form.
 */
function coull_vidlinkr_admin_form_validate($form, &$form_state) {
  $vidlinkr = new Vidlinkr();

  // Attempt to log in and fetch a private key based on the provided credentials
  if(isset($form_state['values']['api_authentication']['email']) && isset($form_state['values']['api_authentication']['email'])) {
    try {
      $form_state[Vidlinkr::CONFIG_KEY_PRIVATE_API_KEY] = $vidlinkr->getPrivateApiKeyFromLogin($form_state['values']['api_authentication']['email'], $form_state['values']['api_authentication']['password']);
    }
    catch (Vidlinkr_Exception $e) {
      form_set_error('api_authentication][email');
      form_set_error('api_authentication][password', $e->getMessage());
    }
  }
}

/**
 * Submit callback for the Vidlinkr admin form.
 */
function coull_vidlinkr_admin_form_submit($form, $form_state) {

  // We're submitting new account details here, so initialise an empty Vidlinkr
  // object instead of passing it config as found elsewhere
  $vidlinkr = new Vidlinkr();

  try {

    // Fetch the private key that we already worked out in the form validation callback
    $vidlinkr->config[Vidlinkr::CONFIG_KEY_PRIVATE_API_KEY] = $form_state[Vidlinkr::CONFIG_KEY_PRIVATE_API_KEY];

    // Grab the full URL to this website
    global $base_url;
    $site_url = $base_url . base_path();

    // Fetch the details for the website, if it already exists in the user's Vidlinkr account
    if (!$vidlinkr->getWebsite($site_url)) {

      // If it doesn't exist, add the website to their Vidlinkr account
      $vidlinkr->addWebsite($site_url);
    }

    // Store the provided email on the Vidlinkr config
    $vidlinkr->config[Vidlinkr::CONFIG_KEY_USER_LOGIN_ID] = $form_state['values']['api_authentication']['email'];

    // Once we've collected everything up, store the variable
    variable_set('coull_vidlinkr_config', $vidlinkr->config);

    drupal_set_message(t('The configuration options have been saved.'));

  }
  catch (Vidlinkr_Exception $e) {

    // Saving the config has failed somewhere in the Vidlinkr code, output a message about it
    drupal_set_message($e->getMessage(), 'warning');

    // Also log this exception to watchdog so that site admins can track it if necessary
    watchdog_exception('coull_vidlinkr', $e);

    return;
  }
}

/**
 * Disconnect callback for the Vidlinkr admin form
 */
function coull_vidlinkr_admin_form_disconnect($form, $form_state) {
  // Delete our config variable
  variable_del('coull_vidlinkr_config');
  drupal_set_message(t('Your settings were cleared'));
}
