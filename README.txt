Coull Vidlinkr is an easy and free way to make money from the YouTube videos you
share on your Drupal site. It adds a Vidlink - a clickable button that connects
your viewers with relevant products/offers - to each of your videos. When a
viewer clicks through or buys, you get paid!

How does it work?
-----------------
Once you’ve installed our Drupal module, Coull Vidlinkr recognises any YouTube
videos on your website and pulls them into your account. You'll see them in a
list and have the option to choose which products/offers to add as a Vidlink to
each video, or let Vidlinkr automatically match your videos to relevant
products/offers.

Alternatively, you can choose not to have a Vidlink on a particular video - you
have complete control.

How much does it cost?
----------------------
Coull Vidlinkr is completely free. Free to sign up and no hidden charges down
the line.

Features
--------
* Easy to set up - Simple step by step instructions
* Easy to maintain - You have full control of how your videos are monetized through our intuitive dashboard
* Unlimited, incremental revenue - Vidlinkr does not interfere with any other monetization methods you might be using with your video content, such as pre-roll ads or AdSense for Video. It’s an additional revenue stream.

FAQ
---
Please visit https://support.coull.com/categories/20088422-Using-Vidlinkr for our more information.

Support
-------
https://support.coull.com/?utm_campaign=drupal&utm_medium=vidlinkr.plugin&utm_source=drupal
